MATLAB MPI
==========

This small helper projects enables parallel MATLAB execution without the use of
the Parallel Computing Toolbox. Think of it as MPI for MATLAB, where it should
be compatible with MPI implementations (tested with OpenMPI and MPICH2).

Build
-----

A common setup (where machines are similar and are binary compatible) would look like:

    $ cd matlab_mpi/build
    $ mkdir _default && cd _default
    $ env CXX=mpicxx cmake -DCMAKE_BUILD_TYPE=Release ../..

For every other machine create a folder with the hostname of the specific host
instead of `_default`.

Example
-------
In the `test` folder there is a `test.sh` example starter and an example matlab
file making use of the MPI code.
