function mpi_send(target, tag, data)
  global mpiCommunicator

  serialized = serialize(data);

  fwrite(mpiCommunicator.there, 'send', 'char');
  fwrite(mpiCommunicator.there, int32(target), 'int32');
  fwrite(mpiCommunicator.there, int32(tag), 'int32');
  fwrite(mpiCommunicator.there, int32(length(serialized)), 'int32');

  fwrite(mpiCommunicator.there, serialized, 'uint8');
end
