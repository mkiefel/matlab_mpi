function mpi_init(thereFilename, backFilename)
  global mpiCommunicator

  fprintf('open back (%s)...', backFilename);
  mpiCommunicator.back = fopen(backFilename, 'r');
  fprintf('done\n');

  fprintf('open there (%s)...', thereFilename);
  mpiCommunicator.there = fopen(thereFilename, 'w');
  fprintf('done\n');
end
