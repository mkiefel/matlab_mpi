function r = mpi_rank()
  global mpiCommunicator

  fwrite(mpiCommunicator.there, 'rank', 'char');
  r = fread(mpiCommunicator.back, 1, 'int32');
