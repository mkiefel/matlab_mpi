function p = mpi_is_init()
  global mpiCommunicator

  p = ~isempty(mpiCommunicator);
