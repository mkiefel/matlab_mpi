function [source, tag, data] = mpi_recv()
  global mpiCommunicator

  fwrite(mpiCommunicator.there, 'recv', 'char');
  source = fread(mpiCommunicator.back, 1, 'int32');
  tag = fread(mpiCommunicator.back, 1, 'int32');
  len = fread(mpiCommunicator.back, 1, 'int32');

  serialized = uint8(fread(mpiCommunicator.back, len, 'uint8'));
  data = deserialize(serialized);
end
