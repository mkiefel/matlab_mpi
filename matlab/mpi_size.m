function s = mpi_size()
  global mpiCommunicator

  fwrite(mpiCommunicator.there, 'size', 'char');
  s = fread(mpiCommunicator.back, 1, 'int32');
