function mpi_finish()
  global mpiCommunicator

  if ~isempty(mpiCommunicator)
    fwrite(mpiCommunicator.there, 'fnsh', 'char');
    % wait for the bridge to be torn down
    fread(mpiCommunicator.back, 1, 'int32');

    fclose(mpiCommunicator.there);
    fclose(mpiCommunicator.back);

    mpiCommunicator = [];
  end
