#!/bin/bash

hostname=`hostname -s`
dir="$( cd "$( dirname "$0" )" && pwd )"

if [ ! -d "${dir}/${hostname}" ]; then
  hostname='_default'
fi

echo "${hostname}"
"${dir}/${hostname}/src/bridge" "$@"
