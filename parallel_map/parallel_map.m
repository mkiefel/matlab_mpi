classdef parallel_map < handle
  properties (GetAccess='public', SetAccess='private')
    iteration
    slaveContinuation
    isParallel
    isWorker

    KILL_TAG
    WORK_TAG
  end

  methods
    function self = parallel_map(slaveContinuation)
      % we need to know if mpi already ran -- then all workers are already dead
      % and we have to opt for sequential calculations
      global parallelMapData

      self.KILL_TAG = 0;
      self.WORK_TAG = 1;

      self.iteration = 0;

      self.isWorker = false;

      if ~isfield(parallelMapData, 'mpiRan')
        parallelMapData.mpiRan = false;
      end

      self.slaveContinuation = slaveContinuation;

      self.isParallel = ~parallelMapData.mpiRan && mpi_is_init();

      if self.isParallel
        parallelMapData.mpiRan = true;

        self.goParallel();
      end
    end

    function delete(self)
      if ~self.isWorker && self.isParallel
        commSize = mpi_size();

        for i=1:commSize-1
          mpi_send(i, self.KILL_TAG, []);
        end
      end
    end

    function mapped = map(self, list)
      if self.isParallel
        mapped = self.mapParallel(list);
      else
        mapped = self.mapSequential(list);
      end
    end
  end

  methods (Access = private)
    function mapped = mapSequential(self, list)
      mapped = cell(length(list), 1);
      for l = 1:length(list)
        mapped{l} = self.slaveContinuation(list{l});
      end
    end

    function mapped = mapParallel(self, list)
      assert(mpi_is_init());

      mapped = cell(length(list), 1);

      % Get size and rank.
      commSize = mpi_size();

      currentJob = 1;

      pending = 0;

      received = zeros(length(list), 1);

      % kick of jobs on workers
      worker = 1;
      while currentJob <= length(list) && worker < commSize
        request = struct();
        request.job = currentJob;
        request.req = list{currentJob};
        mpi_send(worker, self.WORK_TAG + self.iteration, request);

        fprintf('Submitting job %d to worker %d\n', currentJob, worker);

        currentJob = currentJob + 1;
        worker = worker + 1;
        pending = pending + 1;
      end

      % until we are out of jobs, resubmit to workers if they are done
      while currentJob <= length(list)
        [rank, tag, msg] = mpi_recv();

        % throw away old messages
        if tag - self.WORK_TAG == self.iteration
          job = msg.job;
          result = msg.ans;

          if ~received(job)
            mapped{job} = result;
            fprintf('Collected job %d to worker %d\n', job, rank);

            request = struct();
            request.job = currentJob;
            request.req = list{currentJob};
            mpi_send(rank, self.WORK_TAG + self.iteration, request);

            fprintf('Submitting job %d to worker %d\n', currentJob, rank);

            currentJob = currentJob + 1;

            received(job) = 1;
          end
        else
          fprintf('Warning found stale message (job %d, worker %d)\n', job, rank);
        end
      end

      % collect pending jobs
      worker = 1;
      while worker < pending+1
        [rank, tag, msg] = mpi_recv();

        % throw away old messages
        if tag - self.WORK_TAG == self.iteration
          job = msg.job;
          result = msg.ans;

          if ~received(job)
            mapped{job} = result;
            fprintf('Collected job %d from worker %d (pending %d)\n', ...
              job, rank, pending - worker);

            worker = worker + 1;

            received(job) = 1;
          end
        else
          fprintf('Warning found stale message (iteration %d, job %d, worker %d)\n', ...
            self.iteration, job, rank);
        end
      end

      assert(all(received));

      self.iteration = self.iteration + 1;

      fprintf('parallel_map done\n');
    end

    function result = goParallel(self)
      % Get size and rank.
      commSize = mpi_size();
      myRank = mpi_rank();

      if myRank == 0
        % master

        if commSize < 2
          error('No slaves present\n');
        end
      else
        self.isWorker = true;

        ok = true;
        lastTag = self.WORK_TAG;
        lastJob = 0;

        fprintf('slave ready\n');

        while ok
          % slave
          [rank, tag, msg] = mpi_recv();

          if tag == self.KILL_TAG
            ok = false;
          else % WORK_TAG
            assert(tag >= self.WORK_TAG);

            % not nice but should do the trick
            job = msg.job;
            request = msg.req;

            % ignore old messages
            if tag > lastTag || (tag == lastTag && job > lastJob)
              result = self.slaveContinuation(request);

              answer = struct();
              answer.job = job;
              answer.ans = result;

              mpi_send(0, tag, answer);

              lastTag = tag;
              lastJob = job;
            else
              fprintf('Warning got stale message (iteration %d, job %d)', ...
                tag - self.WORK_TAG, msg.job);
            end
          end
        end

        result = [];

        fprintf('slave %d done\n', myRank);

        mpi_finish();
        exit;
      end
    end
  end
end
