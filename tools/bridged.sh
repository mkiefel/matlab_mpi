#!/bin/bash
if [ "$#" -le 1 ]; then
  echo "Usage:"
  echo "${0} <matlab> <matlab-program> [<matlab-args>]"
  exit 1
fi

dir="$( cd "$( dirname "$0" )" && pwd )"

mpi_path="${dir}/../matlab"
parallel_map_path="${dir}/../parallel_map"
bridge="${dir}/../build/bridge_gate.sh"

matlab=$1
shift
program=$1
shift

there=`mktemp`
back=`mktemp`
pid_file=`mktemp`

# make the named pipes
rm "${there}" "${back}"
mkfifo "${there}" "${back}"

# bridge will return on successful setup of the grid
nice -n 2 "${bridge}" --pid-file "${pid_file}" --there "${there}" --back "${back}"

if [ $? -eq 0 ]; then
  child_pid=`cat "${pid_file}"`

  # kill the bridge in case of an error
  trap "sleep 5 ; echo kill '${child_pid}'; kill '${child_pid}' 2>/dev/null ; rm '${there}' '${back}'" SIGKILL SIGTERM SIGABRT SIGQUIT SIGINT

  "${matlab}" "$@" "-r \"addpath '${mpi_path}' ; addpath '${parallel_map_path}' ; mpi_init('${back}', '${there}') ; ${program}; mpi_finish() ; exit\"" 2>&1

  rm "${there}" "${back}"
fi
