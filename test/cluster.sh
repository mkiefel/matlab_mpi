#!/bin/bash

nproc=2

# mpd master
masterhost=$(hostname -f)
masterport=$(($(id -u)+1024))
mpd --listenport=$masterport --daemon || exit 2

echo "masterhost: ${masterhost}"
echo "masterport: ${masterport}"

# mpd slaves
qsub -hard -l matlab=1 -l h_vmem=3G -cwd -t 1-$((nproc-1)) -b yes -j y -N mpd-slaves mpd --host=${masterhost} --port=${masterport} --noconsole

# wait until all slaves are running
while [[ $(mpdtrace | wc -l) -lt $nproc ]]; do sleep 1; done

# run MD simulation
mpiexec.mpich2 -np $nproc bash test.sh

# stop mpd
mpdallexit
