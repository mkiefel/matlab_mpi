#!/bin/bash
#
# e.g. run with
# mpirun -np 4 bash parallelMap_job.sh

program="parallel_map_test"
matlab="matlab"

../tools/bridged.sh "${matlab}" "${program}" -nosplash -nodesktop -singleCompThread
