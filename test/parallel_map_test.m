function parallel_map_test()
  mapper = parallel_map(@slave);

  result = master(mapper);

  result
  assert(result == 20200);

  pause(10);
  fprintf('master: finish\n');

function result = master(mapper)
  jobs = cell(100, 1);
  for i=1:length(jobs)
    jobs{i} = i;
  end
  mapped = mapper.map(jobs);

  result = 0;
  for i = 1:length(mapped)
    result = result + mapped{i};
  end

  jobs = cell(100, 1);
  for i=1:length(jobs)
    jobs{i} = i;
  end
  mapped = mapper.map(jobs);

  for i = 1:length(mapped)
    result = result + mapped{i};
  end

function result = slave(i)
  fprintf('slave: job %d\n', i);
  result = 2 * i;
