#include <mpi.h>

#include <boost/array.hpp>
#include <boost/program_options.hpp>
namespace po = boost::program_options;
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
namespace fs = boost::filesystem;

#include <sys/types.h>
#include <unistd.h>

#include <string>
#include <sstream>
#include <iostream>
#include <cstdint>

class Application {
  private:
    bool verbose_;

  public:
    Application(bool verbose) : verbose_(verbose) {
    }

    void run(const fs::path& filenameThere, const fs::path& filenameBack) {
      int size, rank;
      MPI_Comm_size(MPI_COMM_WORLD, &size);
      MPI_Comm_rank(MPI_COMM_WORLD, &rank);

      std::cout << "MPI: found " << size << " processes" << std::endl;

      if (verbose_)
        std::cout << "open there..." << std::flush;
      fs::ofstream there(filenameThere, std::ios::app);
      if (verbose_)
        std::cout << "done" << std::endl;

      if (verbose_)
        std::cout << "open back..." << std::flush;
      fs::ifstream back(filenameBack);
      if (verbose_)
        std::cout << "done" << std::endl;

      std::string command;
      bool ok = true;

      while (ok) {
        boost::array<char, 5> rawCommand = {{0, 0, 0, 0, 0}};
        // receive commands via the named pipe
        if (verbose_)
          std::cout << rank << ": waiting" << std::endl;
        back.read(rawCommand.data(), 4);

        std::string command = rawCommand.data();

        if (!back.good()) {
          back.close();

          std::cout << rank << ": broken pipe" << std::endl;
          ok = false;
        } else {
          if (verbose_)
            std::cout << rank << ": " << command << std::endl;
          if (command == "recv") {
            MPI_Status msg;
            MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &msg);
            int msgLength;
            MPI_Get_count(&msg, MPI_CHAR, &msgLength);

            std::vector<char> req(msgLength);
            MPI_Recv(req.data(), msgLength, MPI_CHAR, msg.MPI_SOURCE,
                msg.MPI_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

            int32_t source = msg.MPI_SOURCE;
            int32_t tag = msg.MPI_TAG;
            int32_t length = req.size();

            there.write(reinterpret_cast<char*>(&source), sizeof(source));
            there.write(reinterpret_cast<char*>(&tag), sizeof(tag));
            there.write(reinterpret_cast<char*>(&length), sizeof(length));
            there.write(req.data(), req.size());
            there.flush();
          } else if (command == "send") {
            int32_t target, tag, length;

            back.read(reinterpret_cast<char*>(&target), sizeof(target));
            back.read(reinterpret_cast<char*>(&tag), sizeof(tag));
            back.read(reinterpret_cast<char*>(&length), sizeof(length));

            std::vector<char> req(length);
            back.read(req.data(), length);

            MPI_Send(req.data(), length, MPI_CHAR, target, tag, MPI_COMM_WORLD);
          } else if (command == "size") {
            int32_t size32 = size;
            there.write(reinterpret_cast<char*>(&size32), sizeof(size32));
            there.flush();
          } else if (command == "rank") {
            int32_t rank32 = rank;
            there.write(reinterpret_cast<char*>(&rank32), sizeof(rank32));
            there.flush();
          } else if (command == "fnsh") {
            ok = false;

            // call (possibly) blocking finalize
            int32_t result = MPI_Finalize();
            there.write(reinterpret_cast<char*>(&result), sizeof(result));
          } else {
            std::cout << "Error got command " << command << std::endl;

            throw std::runtime_error("Application::run: unknown command");
          }
        }
      }
    }
};

int main(int argc, char* argv[]) {
  // set up mpi
  std::cout << "MPI: init..." << std::endl;
  MPI_Init(&argc, &argv);
  std::cout << "MPI: init done" << std::endl;

  po::options_description generic("Generic options");
  generic.add_options()
    ("there", po::value<std::string>(), "there named pipe")
    ("back", po::value<std::string>(), "back named pipe")
    ("pid-file", po::value<std::string>(), "pid file of forked process")
    ("verbose", "be chatty")
    ("help", "produce help message")
    ;

  po::options_description cmdline_options;
  cmdline_options.add(generic);

  std::ostringstream description;
  description << "Usage for `" << argv[0] << "'";
  po::options_description visible(description.str());
  visible.add(generic);

  po::positional_options_description p;
  p.add("input-img", -1);

  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).
      options(cmdline_options).positional(p).run(), vm);
  po::notify(vm);

  if (vm.count("help") || !vm.count("there") || !vm.count("back")) {
    std::cout << visible << "\n";
    return 1;
  }

  // now fork so that the parent process can exit
  pid_t pid = fork();
  if (pid < 0) {
    std::cout << "fork failed" << std::endl;

    MPI_Finalize();

    return 1;
  } else if (pid == 0) {
    // we are in the child
    Application app(vm.count("verbose") > 0);

    app.run(vm["there"].as<std::string>(), vm["back"].as<std::string>());
  } else if (vm.count("pid-file")) {
    // parent write the pid into the given file
    fs::ofstream pidFile(vm["pid-file"].as<std::string>());
    pidFile << pid;
    pidFile.close();
  }

  return 0;
}
